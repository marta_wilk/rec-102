var app = angular.module('myApp',['ui.sortable']);
            
app.controller('sortCrtl', function($scope){

    var originalScreens = [
        {
            type: 0,
            title: "Input - text element",
            name: "Name of element",
            placeholder: "Placeholder",
            class: "cssClass",
            ownclass: "input"
        },
        {
            type: 1,
            title: "Text area",
            name: "Name of element",
            placeholder: "Placeholder",
            class: "cssClass",
            ownclass: "textarea"
        },
        {
            type: 2,
            title: "Button",
            name: "Send",
            class: "cssClass",
            formaction: "",
            ownclass: "button"
        }
    ];

    $scope.formconfig = [];
    $scope.sourceScreens = originalScreens.slice(0);
    $scope.resultScreens =  [];
    $scope.output = [$scope.formaction, $scope.resultScreens];
    
    $scope.sortableOptions = {
        connectWith: ".connected-apps-container",
        receive: function(e, ui) {
        },
        stop: function(e, ui) {;
            $scope.sourceScreens = originalScreens.slice(0);
            itterateResults();
        }
    };
    
    $scope.removeElement = function(index) {
        console.log($scope.resultScreens[index]);
        delete $scope.resultScreens[index];
        itterateResults();
    }
    
    function itterateResults() {
        for (index = 0; index < $scope.resultScreens.length; ++index) {
            var element = $scope.resultScreens[index];
            if (element != undefined) {
                $scope.resultScreens[index] = jQuery.extend({}, element);
            }
        }
        $scope.resultScreens = $scope.resultScreens.filter(function(){return  true;});
    }
});

